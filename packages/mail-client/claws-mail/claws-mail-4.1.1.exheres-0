# Copyright 2008, 2009, 2010 Mike Kelly
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require gtk-icon-cache freedesktop-desktop
require python [ blacklist=3 multibuild=false with_opt=true ]

SUMMARY="Email client and news reader based on GTK+"
DESCRIPTION="
Claws Mail is an email client and news reader based on GTK+, featuring:
* Quick response
* Graceful and sophisticated interface
* Easy configuration and intuitive operation
* Abundant features
* Extensibility
* Robustness and stability
"
HOMEPAGE="https://www.claws-mail.org/"
DOWNLOADS="${HOMEPAGE}download.php?file=releases/${PNV}.tar.xz"

UPSTREAM_CHANGELOG="https://git.claws-mail.org/?p=claws.git;a=blob_plain;f=NEWS;hb=HEAD"
UPSTREAM_DOCUMENTATION="
https://www.claws-mail.org/faq/index.php/Main_Page [[ description = [ FAQ ] ]]
https://www.claws-mail.org/documentation.php?section=general [[ description = [ User Manual ] ]]
"
UPSTREAM_RELEASE_NOTES="https://www.claws-mail.org/news.php"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    archive         [[ description = [ Enable the archive plug-in ] ]]
    bogofilter      [[ description = [ Enable bogofilter anti-spam ] ]]
    dbus
    ldap
    libnotify
    networkmanager  [[ requires = dbus ]]
    pdf             [[ description = [ Enable PDF plug-in ] ]]
    pgp             [[ description = [ Enable PGP/SMIME plug-in ] ]]
    python          [[ description = [ Enable Python console/scripts plugin ] ]]
    rss [[ description = [ Enable RSS newsfeed plugin ] ]]
    spell
    startup-notification
    svg
    vcalendar       [[ description = [ Enable VCalendar/iCal plugin ] ]]
    ( linguas: ca cs da de el_GR en_GB es fi fr hu id_ID it ja nb nl pl pt_BR pt_PT ro ru sk sv tr zh_TW )
"

# fails to build, last checked: 3.17.4
RESTRICT="test"

DEPENDENCIES="
    build:
        app-arch/xz
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libarchive
        dev-libs/glib:2[>=2.36]
        dev-libs/gnutls[>=3.0] [[ note = [ 3.0 for password encryption ] ]]
        dev-libs/nettle:=
        mail-libs/libetpan[>=1.9.4]
        net-misc/curl
        x11-libs/cairo[>=1.0]
        x11-libs/gdk-pixbuf:2.0[>=2.26]
        x11-libs/gtk+:3[>=3.20]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/pango [[ note = [ automagic ] ]]
        archive? ( app-arch/libarchive )
        bogofilter? ( mail-filter/bogofilter )
        dbus? (
            dev-libs/dbus-glib:1[>=0.60]
            sys-apps/dbus[>=0.60]
        )
        ldap? ( net-directory/openldap )
        libnotify? (
            x11-libs/libnotify[>=0.4.3]
            media-libs/libcanberra[>=0.6][providers:gtk3]
        )
        networkmanager? ( net-apps/NetworkManager[>=0.6.2] )
        pdf? ( app-text/poppler[>=0.12.0][cairo(+)][glib] )
        pgp? ( app-crypt/gpgme[>=1.1.1] )
        python? ( gnome-bindings/pygobject:3 )
        rss? ( dev-libs/expat )
        spell? ( app-spell/enchant:2[>=2.0.0] )
        startup-notification? ( x11-libs/startup-notification[>=0.5] )
        svg? ( gnome-desktop/librsvg:2[>=2.39.0] )
        vcalendar? ( office-libs/libical )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ### Plugins
    --enable-acpi_notifier-plugin
    --enable-archive-plugin
    --enable-address_keeper-plugin
    --enable-alternate-addressbook
    --enable-att_remover-plugin
    --enable-attachwarner-plugin
    --enable-fetchinfo-plugin
    --enable-keyword_warner-plugin
    --enable-mailmbox-plugin
    --enable-managesieve-plugin
    --enable-newmail-plugin
    --enable-spam_report-plugin
    --disable-bsfilter-plugin
    --disable-clamd-plugin
    --disable-demo-plugin
    --disable-dillo-plugin
    --disable-fancy-plugin
    --disable-gdata-plugin
    --disable-libravatar-plugin
    --disable-litehtml_viewer-plugin
    --disable-notification-plugin
    --disable-perl-plugin
    --disable-spamassassin-plugin
    --disable-tnef_parse-plugin

    --enable-deprecated # fails to build otherwise
    --enable-gnutls
    --enable-ipv6
    --enable-largefile
    --enable-libsm
    --enable-nls
    --enable-oauth2
    --enable-pthread
    --disable-compface # missing dependency
    --disable-jpilot # missing dependency
    --disable-static
    --disable-valgrind
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'archive archive-plugin'
    'bogofilter bogofilter-plugin'
    'dbus'
    'ldap'
    'libnotify notification-plugin'
    'networkmanager'
    'pdf pdf_viewer-plugin'
    'pgp pgpcore-plugin'
    'pgp pgpinline-plugin'
    'pgp pgpmime-plugin'
    'pgp smime-plugin'
    'python python-plugin'
    'vcalendar vcalendar-plugin'
    'rss rssyl-plugin'
    'spell enchant'
    'startup-notification'
    'svg'
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

