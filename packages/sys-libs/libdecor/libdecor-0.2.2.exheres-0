# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://gitlab.freedesktop.org new_download_scheme=true ] \
    meson

SUMMARY="Client-side decorations library for Wayland clients"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    ( providers: gtk3 )
"

DEPENDENCIES="
    build:
        sys-libs/wayland-protocols[>=1.32]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        sys-apps/dbus[>=1.0]
        sys-libs/wayland[>=1.18]
        x11-libs/cairo
        x11-libs/pango
        providers:gtk3? ( x11-libs/gtk+:3 )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddbus=enabled
    -Ddemo=false
    -Dinstall_demo=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'providers:gtk3 gtk'
)

